package com.taufik.gamelistapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.taufik.gamelistapp.BuildConfig
import com.taufik.gamelistapp.data.remote.response.ResultItems
import com.taufik.gamelistapp.data.remote.retrofit.ApiConfig
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class MainViewModel : ViewModel() {

    private val _responseTopRating = MutableLiveData<List<ResultItems>>()
    val responseTopRating: LiveData<List<ResultItems>> = _responseTopRating

    private val _responseLatestGames = MutableLiveData<List<ResultItems>>()
    val responseLatestGames: LiveData<List<ResultItems>> = _responseLatestGames

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    val disposables: CompositeDisposable = CompositeDisposable()

    fun setTopRating() {
        _isLoading.value = true
        ApiConfig.getApiService()
            .getGames(BuildConfig.API_KEY, 15, "-rating", 4, 1, "2021-12-01,2022-01-01")
            .observeOn(AndroidSchedulers.mainThread()).subscribe({ response ->
                _responseTopRating.postValue(response.results)
                _isLoading.value = false
            }, { error ->
                Log.e("TAG", "onFailure: ${error.message}")
                _isLoading.value = false
            }).addTo(disposables)
    }

    fun setLatestGames() {
        _isLoading.value = true
        ApiConfig.getApiService()
            .getGames(BuildConfig.API_KEY, 15, "-released", 4, 1, "2021-12-01,2022-01-01")
            .observeOn(AndroidSchedulers.mainThread()).subscribe({ response ->
                _responseLatestGames.postValue(response.results)
                _isLoading.value = false
            }, { error ->
                Log.e("TAG", "onFailure: ${error.message}")
                _isLoading.value = false
            }).addTo(disposables)
    }
}
