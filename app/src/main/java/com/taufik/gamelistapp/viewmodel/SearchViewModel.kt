package com.taufik.gamelistapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.taufik.gamelistapp.BuildConfig
import com.taufik.gamelistapp.data.remote.response.ResultItems
import com.taufik.gamelistapp.data.remote.retrofit.ApiConfig
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class SearchViewModel: ViewModel() {

    private val _responseSearch = MutableLiveData<List<ResultItems>>()
    val responseSearch: LiveData<List<ResultItems>> = _responseSearch

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    val disposables: CompositeDisposable = CompositeDisposable()

    fun searchGames(title: String) {
        _isLoading.value = true
        ApiConfig.getApiService()
            .searchGames(BuildConfig.API_KEY, 15, 4, title, true, true,1 )
            .observeOn(AndroidSchedulers.mainThread()).subscribe({ response ->
                _responseSearch.postValue(response.results)
                _isLoading.value = false
            }, { error ->
                Log.e("TAG", "onFailure: ${error.message}")
                _isLoading.value = false
            }).addTo(disposables)
    }

}