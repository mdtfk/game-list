package com.taufik.gamelistapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.taufik.gamelistapp.adapter.SeeMoreLatestAdapter
import com.taufik.gamelistapp.data.remote.response.ResultItems
import com.taufik.gamelistapp.databinding.ActivitySeeMoreLatestBinding
import com.taufik.gamelistapp.viewmodel.MainViewModel

class SeeMoreLatestActivity : AppCompatActivity() {
    private lateinit var binding : ActivitySeeMoreLatestBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySeeMoreLatestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = "Latest Games"

        initViewModel()
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())[MainViewModel::class.java]
        viewModel.setLatestGames()
        viewModel.responseLatestGames.observe(this) {
            setData(it)
        }

        viewModel.isLoading.observe(this) {
            showLoading(it)
        }
    }

    private fun setData(data: List<ResultItems>) {
        val adapter = SeeMoreLatestAdapter(data)
        binding.rvSeeMoreLatest.adapter = adapter

        adapter.setOnItemClickCallback(object : SeeMoreLatestAdapter.OnItemClickCallback {
            override fun onItemClicked(data: ResultItems) {
                val intent = Intent(this@SeeMoreLatestActivity, GamesDetailActivity::class.java)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_TITLE, data.name)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_IMAGE, data.image)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_RELEASED, data.released)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_RATING, data.rating)
                startActivity(intent)
            }
        })
    }

    private fun showLoading(value: Boolean) {
        if (value) {
            binding.progressBarSeeMoreLatest.visibility = View.VISIBLE
            binding.progressBarSeeMoreLatest.visibility = View.VISIBLE
        } else {
            binding.progressBarSeeMoreLatest.visibility = View.GONE
            binding.progressBarSeeMoreLatest.visibility = View.GONE
        }
    }
}