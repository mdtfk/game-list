package com.taufik.gamelistapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.taufik.gamelistapp.adapter.LatestGamesAdapter
import com.taufik.gamelistapp.adapter.TopGamesAdapter
import com.taufik.gamelistapp.data.remote.response.ResultItems
import com.taufik.gamelistapp.databinding.ActivityMainBinding
import com.taufik.gamelistapp.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViewModel()
        initView()

        binding.tvSeeMoreTopRating.setOnClickListener {
            Intent(this, SeeMoreTopActivity::class.java).also {
                startActivity(it)
            }
        }

        binding.tvSeeMoreLatestGame.setOnClickListener {
            Intent(this, SeeMoreLatestActivity::class.java).also {
                startActivity(it)
            }
        }

        binding.fabSearch.setOnClickListener{
            Intent(this, SearchActivity::class.java).also {
                startActivity(it)
            }
        }
    }

    private fun initView() {
        val layoutManagerTop = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        binding.rvTopRating.layoutManager = layoutManagerTop

        val layoutManagerLatest = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        binding.rvLatestGames.layoutManager = layoutManagerLatest
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())[MainViewModel::class.java]
        viewModel.setTopRating()
        viewModel.responseTopRating.observe(this) {
            setRvDataTop(it)
        }
        viewModel.setLatestGames()
        viewModel.responseLatestGames.observe(this) {
            setRvDataLatest(it)
        }
        viewModel.isLoading.observe(this) {
            showLoading(it)
        }
    }

    private fun setRvDataTop(data: List<ResultItems>){
        val topGamesAdapter = TopGamesAdapter(data)
        binding.rvTopRating.adapter = topGamesAdapter

        topGamesAdapter.setOnItemClickCallback(object : TopGamesAdapter.OnItemClickCallback {
            override fun onItemClicked(data: ResultItems) {
                val intent = Intent(this@MainActivity, GamesDetailActivity::class.java)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_TITLE, data.name)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_IMAGE, data.image)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_RELEASED, data.released)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_RATING, data.rating)
                startActivity(intent)
            }
        })
    }

    private fun setRvDataLatest(data: List<ResultItems>){
        val latestGamesAdapter = LatestGamesAdapter(data)
        binding.rvLatestGames.adapter = latestGamesAdapter

        latestGamesAdapter.setOnItemClickCallback(object : LatestGamesAdapter.OnItemClickCallback {
            override fun onItemClicked(data: ResultItems) {
                val intent = Intent(this@MainActivity, GamesDetailActivity::class.java)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_TITLE, data.name)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_IMAGE, data.image)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_RELEASED, data.released)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_RATING, data.rating)
                startActivity(intent)
            }
        })
    }

    private fun showLoading(value: Boolean) {
        if (value) {
            binding.progressBarTop.visibility = View.VISIBLE
            binding.progressBarLatest.visibility = View.VISIBLE
        } else {
            binding.progressBarTop.visibility = View.GONE
            binding.progressBarLatest.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.disposables.clear()
    }
}