package com.taufik.gamelistapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.taufik.gamelistapp.databinding.ActivityGamesDetailBinding

class GamesDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityGamesDetailBinding

    companion object {
        const val EXTRA_GAME_TITLE = "extra_game_title"
        const val EXTRA_GAME_IMAGE = "extra_game_image"
        const val EXTRA_GAME_RELEASED = "extra_game_released"
        const val EXTRA_GAME_RATING= "extra_game_rating"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGamesDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val gameTitle = intent.getStringExtra(EXTRA_GAME_TITLE)
        val gameImage = intent.getStringExtra(EXTRA_GAME_IMAGE)
        val gameReleased = intent.getStringExtra(EXTRA_GAME_RELEASED)
        val gameRating = intent.getDoubleExtra(EXTRA_GAME_RATING, 0.0)

        supportActionBar?.title = gameTitle

        setGameDetail(gameTitle, gameImage, gameReleased, gameRating)
    }

    private fun setGameDetail(gameTitle: String?, gameImage: String?, gameReleased: String?, gameRating: Double?) {
        Glide.with(this).load(gameImage).transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.imgDetail)
        binding.tvTitleDetail.text = gameTitle
        binding.tvReleasedDateDetail.text = gameReleased
        binding.tvRatingDetail.text = gameRating.toString()
    }
}