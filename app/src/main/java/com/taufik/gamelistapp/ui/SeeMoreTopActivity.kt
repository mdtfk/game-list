package com.taufik.gamelistapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.taufik.gamelistapp.adapter.SeeMoreTopAdapter
import com.taufik.gamelistapp.data.remote.response.ResultItems
import com.taufik.gamelistapp.databinding.ActivitySeeMoreTopBinding
import com.taufik.gamelistapp.viewmodel.MainViewModel

class SeeMoreTopActivity : AppCompatActivity() {
    private lateinit var binding : ActivitySeeMoreTopBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySeeMoreTopBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = "Top Rating Games"

        initViewModel()
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())[MainViewModel::class.java]
        viewModel.setTopRating()
        viewModel.responseTopRating.observe(this) {
            setData(it)
        }

        viewModel.isLoading.observe(this) {
            showLoading(it)
        }
    }

    private fun setData(data: List<ResultItems>) {
        val adapter = SeeMoreTopAdapter(data)
        binding.rvSeeMore.adapter = adapter

        adapter.setOnItemClickCallback(object : SeeMoreTopAdapter.OnItemClickCallback {
            override fun onItemClicked(data: ResultItems) {
                val intent = Intent(this@SeeMoreTopActivity, GamesDetailActivity::class.java)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_TITLE, data.name)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_IMAGE, data.image)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_RELEASED, data.released)
                intent.putExtra(GamesDetailActivity.EXTRA_GAME_RATING, data.rating)
                startActivity(intent)
            }
        })
    }

    private fun showLoading(value: Boolean) {
        if (value) {
            binding.progressBarSeeMore.visibility = View.VISIBLE
            binding.progressBarSeeMore.visibility = View.VISIBLE
        } else {
            binding.progressBarSeeMore.visibility = View.GONE
            binding.progressBarSeeMore.visibility = View.GONE
        }
    }
}