package com.taufik.gamelistapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.jakewharton.rxbinding4.widget.textChanges
import com.taufik.gamelistapp.adapter.SearchAdapter
import com.taufik.gamelistapp.data.remote.response.ResultItems
import com.taufik.gamelistapp.databinding.ActivitySearchBinding
import com.taufik.gamelistapp.viewmodel.SearchViewModel

class SearchActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySearchBinding
    private lateinit var viewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        showLoading(false)

        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())[SearchViewModel::class.java]

        binding.etSearch.textChanges().skipInitialValue().map {
            return@map StringBuilder(it).toString()
        }.subscribe {
            viewModel.searchGames(it)
            viewModel.responseSearch.observe(this) { result ->
                val adapter = SearchAdapter(result)
                binding.rvSearch.adapter = adapter

                adapter.setOnItemClickCallback(object : SearchAdapter.OnItemClickCallback {
                    override fun onItemClicked(data: ResultItems) { val intent = Intent(this@SearchActivity, GamesDetailActivity::class.java)
                        intent.putExtra(GamesDetailActivity.EXTRA_GAME_TITLE, data.name)
                        intent.putExtra(GamesDetailActivity.EXTRA_GAME_IMAGE, data.image)
                        intent.putExtra(GamesDetailActivity.EXTRA_GAME_RELEASED, data.released)
                        intent.putExtra(GamesDetailActivity.EXTRA_GAME_RATING, data.rating)
                        startActivity(intent)
                    }
                })
            }
            viewModel.isLoading.observe(this) {
                showLoading(it)
            }
        }
    }

    private fun showLoading(value: Boolean) {
        if (value) {
            binding.progressBarSearch.visibility = View.VISIBLE
            binding.progressBarSearch.visibility = View.VISIBLE
        } else {
            binding.progressBarSearch.visibility = View.GONE
            binding.progressBarSearch.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.disposables.clear()
    }
}