package com.taufik.gamelistapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.taufik.gamelistapp.R
import com.taufik.gamelistapp.data.remote.response.ResultItems
import kotlinx.android.synthetic.main.item_see_more.view.*

class SeeMoreTopAdapter(val results: List<ResultItems>): RecyclerView.Adapter<SeeMoreTopAdapter.ViewHolder>() {
    private lateinit var onItemClickCallback: OnItemClickCallback

    interface OnItemClickCallback {
        fun onItemClicked(data: ResultItems)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        LayoutInflater.from(parent.context).inflate(R.layout.item_see_more, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = results[position]
        holder.view.tv_see_more_title.text = result.name
        Glide.with(holder.view.context).load(result.image)
            .transition(DrawableTransitionOptions.withCrossFade()).into(holder.view.img_see_more)

        holder.view.img_see_more.setOnClickListener{
            onItemClickCallback.onItemClicked(results[holder.adapterPosition])
        }
    }

    override fun getItemCount() = results.size


    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    class ViewHolder (val view: View): RecyclerView.ViewHolder(view)

}