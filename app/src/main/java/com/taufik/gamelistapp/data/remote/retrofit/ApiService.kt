package com.taufik.gamelistapp.data.remote.retrofit

import com.taufik.gamelistapp.data.remote.response.Games
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("games")
    fun getGames(
        @Query("key") key: String,
        @Query("page_size") pageSize: Int,
        @Query("ordering") ordering: String,
        @Query("platforms") platforms: Int,
        @Query("page") page: Int,
        @Query("dates") dates: String,
    ) : Single<Games>

    @GET("games")
    fun searchGames(
        @Query("key") key: String,
        @Query("page_size") pageSize: Int,
        @Query("platforms") platforms: Int,
        @Query("search") search: String,
        @Query("search_precise") searchPrecise: Boolean,
        @Query("search_exact") searchExact: Boolean,
        @Query("page") page: Int,
    ) : Single<Games>
}