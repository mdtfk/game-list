package com.taufik.gamelistapp.data.remote.response

import com.google.gson.annotations.SerializedName

data class Games(
    @field:SerializedName("results")
    val results: List<ResultItems>
)

data class ResultItems(
    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("released")
    val released: String,

    @field:SerializedName("background_image")
    val image: String,

    @field:SerializedName("rating")
    val rating: Double,
)